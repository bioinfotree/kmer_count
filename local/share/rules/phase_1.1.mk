# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>


# TODO:


context prj/rnaseq_assembly

# remove count for kmers with frequence less then indicated
MIN_OCC	?= 3
# kmers length; passed by supra-makefile
KMER	?= 16
# fastq sequences, gz compressed
SEQ_GZ	?=
# estimated genome size
G_SIZE ?= 400879909
# define the hash size for jellyfish #9293715826
HASH_SIZE ?= 
# max number of occurence in kmer count
MAX_OCC ?= 10000
# expected depth for unique kmer
EXPECT_KDEPTH ?=
# execute random sobstitution of N in sequences
RANDOM_SUB_N ?= FALSE


# # make temporary directory
# tmp_dir.mk:
# 	TMPDIR=$$(mktemp -d tmpXXXX || { echo "Failed to create temp dir"; exit 1;}); \
# 	printf "TMPDIR := %s" $$TMPDIR >$@;
# 	
# 
# include tmp_dir.mk



tmp:
	mkdir -p $@ || { echo "Failed to create temp dir"; exit 1;}


SEQ_FASTQ_N = $(wildcard *.fastq.gz)


ifeq ($(RANDOM_SUB_N),TRUE)
SEQ_FASTQ_N = $(wildcard *.fastq.N)
endif

hash_size.mk: $(SEQ_FASTQ_N)
ifndef HASH_SIZE
	READS=$$(cat $^ | fasta2tab | wc -l); \
	HASH_SIZE=$$(echo "( $(G_SIZE) + $(KMER) * $$READS ) / 0.8 " | bc); \
	echo $$HASH_SIZE; \
	printf "HASH_SIZE := %i" $$HASH_SIZE >$@
else
	printf "HASH_SIZE := $(HASH_SIZE)" >$@
endif





include hash_size.mk


ifeq ($(RANDOM_SUB_N),TRUE)
# run JELLYFISH to count kmers
jellyfish.stats: tmp $(SEQ_FASTQ_N)
	!threads
	@echo
	@echo "++ Step 2: Running JELLYFISH to count kmers ..."
	@echo
	$(call module_loader); \
	cat $(call rest,$^) \   * return a list with the first element removed *
	| jellyfish count \
	-m $(KMER) \
	-o $</jf_tmp \
	-s $(HASH_SIZE) \   * ISO suffixes M G are accepted *
	-t $$THREADNUM \
	--stats=$@ \
	--both-strands \
	--lower-count=$(MIN_OCC) \
	--upper-count=$(MAX_OCC) \
	/dev/fd/0
else
jellyfish.stats: tmp $(SEQ_FASTQ_N)
	!threads
	@echo
	@echo "++ Step 2: Running JELLYFISH to count kmers ..."
	@echo
	$(call module_loader); \
	zcat $(call rest,$^) \   * return a list with the first element removed *
	| jellyfish count \
	-m $(KMER) \
	-o $</jf_tmp \
	-s $(HASH_SIZE) \   * ISO suffixes M G are accepted *
	-t $$THREADNUM \
	--stats=$@ \
	--both-strands \
	--lower-count=$(MIN_OCC) \
	--upper-count=$(MAX_OCC) \
	/dev/fd/0
endif




# merge jellyfish  databases
jf_merged_$(KMER): tmp jellyfish.stats
	@echo
	@echo "++ Step 3: Running JELLYFISH to merge count kmers ..."
	@echo
	if [ -s $</jf_tmp_0 ]; then \
	N_TMP=$$(ls -1 $</jf_tmp_* | wc -l); \
	if [ $$N_TMP -eq 1 ]; then \
	mv $</jf_tmp_0 $@; \
	else \
	$(call module_loader); \
	jellyfish merge $</jf_tmp_* -o $@; \
	fi; \
	fi



# data for plot histogram
jellyfish.histo: jf_merged_$(KMER)
	!threads
	@echo
	@echo "++ Step 4: Running JELLYFISH to create hostogram of k-mer occurence ..."
	@echo
	$(call module_loader); \
	jellyfish histo \
	-o $@ \
	-t $$THREADNUM \
	$^


# # ad 3° column in which calculate the product of frequency * kmer_species
# jellyfish.histo.count: jellyfish.histo
# 	awk 'BEGIN{ FS=" "; OFS="\t";} !/^[$$,\#+]/ { print $$1,$$2,$$1 * $$2; }' <$< >$@


# columns in kmer spectrum files jellyfish.histo.count
# 1: kmer_frequency
# 2: num_distinct_kmers
# 3: frac_distinct_kmers = (2)/num_distinct_kmers_total
# 4: cummulative(2)
# 5: cummulative(3)      = (4)/num_distinct_kmers_total
# 6: num_kmers           = (1)*(2)
# 7: frac_kmers          = (6)/num_kmers_total
# 8: cummulative(6)
# 9: cummulative(7)      = (8)/num_kmers_total
jellyfish.histo.count: jellyfish.histo
	TOTAL_NUM_DISTINCT_KMERS=$$(awk 'BEGIN{ FS=" "; OFS="\t"; tot=0;} !/^[$$,\#+]/ { tot += $$2; } END {print tot;}' <$<); \
	TOTAL_NUM_KMERS=$$(awk 'BEGIN{ FS=" "; OFS="\t"; tot=0;} !/^[$$,\#+]/ { tot += $$1*$$2; } END {print tot;}' <$<); \
	awk -v total_num_distinct_kmers=$$TOTAL_NUM_DISTINCT_KMERS \
	    -v total_num_kmers=$$TOTAL_NUM_KMERS \
	'BEGIN{ printf "# 1:kmer_frequency\t2:num_distinct_kmers\t3:2/total_num_distinct_kmers\t4:cummulative(2)\t5:4/total_num_distinct_kmers\t6:num_kmers (1)*(2)\t7:6/total_num_kmers\t8:cummulative(6)\t9:8/total_num_kmers\n"; \
		_4=0; \
	        _8=0; \
	        FS=" "; \
	        OFS="\t"; } \
	!/^[$$,\#+]/ \
	{ kmer_frequency = $$1; \
	  num_distinct_kmers = $$2; \
	  _3 = num_distinct_kmers / total_num_distinct_kmers; \
	  _4 += num_distinct_kmers; \
	  _5 = _4 / total_num_distinct_kmers; \
	  _6 = kmer_frequency * num_distinct_kmers; \
	  _7 = _6 / total_num_kmers; \
	  _8 += _6; \
	  _9 = _8 / total_num_kmers; \
	  print kmer_frequency, num_distinct_kmers, _3, _4, _5, _6, _7, _8, _9; \
	}' <$< >$@



# # plot histogram in R
# jellyfish.histo.count.pdf: jellyfish.histo.count
# 	$(call module_loader); \
# 	 distrib_plot --type=histogram --breaks=$(MAX_OCC) --output=$@ 3 <$<




# more complete statistics
jellyfish.stats.verbose: jf_merged_$(KMER)
	!threads
	@echo
	@echo "++ Step 5: Running JELLYFISH to create statistics of k-mer occurence ..."
	@echo
	$(call module_loader); \
	jellyfish stats \
	-o $@ \
	$<


# dump lower counts (needed for seecer)
counts_$(KMER)_$(MIN_OCC): jf_merged_$(KMER)
	@echo
	@echo "++ Step 6: Running JELLYFISH to dump k-mer occurence ..."
	@echo
	$(call module_loader); \
	jellyfish dump --lower-count=$(MIN_OCC) -c $< -o $@;



# count only 16-mers. It is very fast because count in memory.
# produce output in 3 columns: take 1 and 3 for produce the histogram
NOTPARALLEL: kmers_count.hist
kmers_count.hist: $(SEQ_FASTQ) jellyfish.stats.verbose
	!threads
	$(call module_loader); \
	kmers_count $(addprefix --input ,$^) --threads $$THREADNUM --max-occ $(MAX_OCC) --16mer-hist $@


# print histogram
kmers_count.hist.pdf: kmers_count.hist
	$(call module_loader); \
	head -n -1 $< | distrib_plot --type=histogram --breaks=$(MAX_OCC) --output=$@ 3


# test_kmer_freq_pread:
# 	kmer_freq_pread -k 16 -l <(echo -e "0.fastq\n1.fastq\n2.fastq") -t 20 -p $@_k17  2>$@_k17.log



# -f <string> depth frequency file; select columns 1 and 2 from jellyfish.histo.count, must be tab separated
# -c      <int>   expected depth for unique kmer; set if you whant a proper genome size. You can obtain the value from the output of kmer frequence count programs or you can use the  # 
# -M      <int>   max depth value, default=256; you must set it
# -b      <int>   have bias(1) or not(0), default=0 estimate genome size in gce.log; when the K < 19 for human, do not set it when K>19 set it for real sequencing data.
# For E.coli, because the species is quite smaller than 17, you need to set it for real sequencing data
# -H      <int>   use hybrid mode(1) or not(0), default=0; To estimate the heterozygous ratio set -H 1 -c unique_depth. Gce estimate Kmer Heterozygous Ratio(KHR) based on the formula: KHR = a1/2/(2-a1/2). To get the Base Heterozygous Ratio(BHR), you can use the formula: BHR = KHR/Kmer(suppose each snp cause K new kmers).
# 5. How to decide whether use continuous model or discrete model? The performance of the two model is quite similar. For real sequencing data, continuous model is more suitable, however, its stability should be improved.
gce.log: jellyfish.histo jellyfish.stats
ifdef EXPECT_KDEPTH
	gce -f <$< \
	-M $(MAX_OCC) \
	-g <(grep -w "Total:" <jellyfish.stats | tr -s ' ' \\t | cut -f2) \
	-c 91  \
	-H 1 \
	>$(basename $@).table 2>$@
else
	gce -f <$< \
	-M $(MAX_OCC) \
	-g <(grep -w "Total:" <jellyfish.stats | tr -s ' ' \\t | cut -f2) \
	>$(basename $@).table 2>$@

endif




.PHONY: test
test:
	@echo $(wildcard *.fastq.N)



ALL +=	$(SEQ_FASTQ_N) \
	hash_size.mk \
	jf_merged_$(KMER) \
	jellyfish.stats \
	jellyfish.histo \
	jellyfish.histo.count \
	jellyfish.stats.verbose \
	gce.log
#	jellyfish.histo.count.pdf \
#	counts_$(KMER)_$(MIN_OCC) \




# ifeq ($(KMER),16)
# ALL +=	kmers_count.hist \
# 	kmers_count.hist.pdf
# endif


INTERMEDIATE += 

#$(wildcard *.fastq) \
#		$(SEQ_FASTQ) \
#		$(SEQ_FASTQ_N)

CLEAN +=  tmp \
	  gce.table
