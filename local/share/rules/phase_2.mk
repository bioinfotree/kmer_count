# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>


# TODO:

MAX_OCC_IN_GRAPHS ?= $(MAX_OCC)

# this line guarantie that the phase_2 is executed when phase_1 is finished
extern ../phase_1/subsystem.flag as SUBSYSTEM_FLAG

dir_lst.mk: $(SUBSYSTEM_FLAG)
ifeq ($(wildcard ../phase_1/dir_lst.mk),)
	touch $@
else
	ln -sf ../phase_1/dir_lst.mk $@
endif


# $(error ERROR! dir_lst.mk cannot be found!)
# else
include dir_lst.mk
#endif


# create links to jellyfish.histo.count for every kmer
.PHONY: link_installer
link_installer: dir_lst.mk
	@echo 
	for D in $(addprefix ../phase_1/,$(DIR_LST)); do \
	  if [ -d $$D ]; then \
	    ln  -sf $$D/jellyfish.histo.count $$(basename $$D)_jellyfish.histo.count; \
	  fi; \
	done


HISTO_LST = $(addsuffix _jellyfish.histo.count,$(DIR_LST))
# needed for usage in merge function
COMMA := ,

# KmerSpectrumPlot.pl is part of ALLPATHS assembly package.
# It creates script for GNUplot and execute it by producing plots in .gif and .eps format
jellyfish.histo.gp: link_installer
	$(call module_loader); \
	KmerSpectrumPlot.pl \
	SPECTRA="{$(call merge,$(COMMA),$(HISTO_LST))}" \
	FREQ_MIN=$(MIN_OCC) \
	FREQ_MAX=$(MAX_OCC_IN_GRAPHS) \
	HEAD_OUT=$(basename $@) \
	GIF=1 \
	ORGANISM=$(PRJ)


.PHONY: test
test:
	@echo $(wildcard dir_lst.mk)







ALL +=	dir_lst.mk \
	link_installer \
	jellyfish.histo.gp


INTERMEDIATE += 

CLEAN += $(wildcard jellyfish.histo.*) \
	 $(wildcard *.count)