# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>


# TODO:


context prj/rnaseq_assembly

# remove count for kmers with frequence less then indicated
MIN_OCC	?= 3
# kmers length; passed by supra-makefile
KMER	?= 16
# max number of occurence in kmer count
MAX_OCC ?= 10000
# expected depth for unique kmer
EXPECT_KDEPTH ?=


# merge jellyfish  databases
jf_merged_$(KMER):
	@echo
	@echo "++ Step 3: Running JELLYFISH to merge count kmers ..."
	@echo
	$(call module_loader); \
	jellyfish merge jf_*_$(KMER) -o $@;



# data for plot histogram
jellyfish.histo: jf_merged_$(KMER)
	!threads
	@echo
	@echo "++ Step 4: Running JELLYFISH to create hostogram of k-mer occurence ..."
	@echo
	$(call module_loader); \
	jellyfish histo \
	-o $@ \
	-t $$THREADNUM \
	$^


# # ad 3° column in which calculate the product of frequency * kmer_species
# jellyfish.histo.count: jellyfish.histo
# 	awk 'BEGIN{ FS=" "; OFS="\t";} !/^[$$,\#+]/ { print $$1,$$2,$$1 * $$2; }' <$< >$@


# columns in kmer spectrum files jellyfish.histo.count
# 1: kmer_frequency
# 2: num_distinct_kmers
# 3: frac_distinct_kmers = (2)/num_distinct_kmers_total
# 4: cummulative(2)
# 5: cummulative(3)      = (4)/num_distinct_kmers_total
# 6: num_kmers           = (1)*(2)
# 7: frac_kmers          = (6)/num_kmers_total
# 8: cummulative(6)
# 9: cummulative(7)      = (8)/num_kmers_total
jellyfish.histo.count: jellyfish.histo
	TOTAL_NUM_DISTINCT_KMERS=$$(awk 'BEGIN{ FS=" "; OFS="\t"; tot=0;} !/^[$$,\#+]/ { tot += $$2; } END {print tot;}' <$<); \
	TOTAL_NUM_KMERS=$$(awk 'BEGIN{ FS=" "; OFS="\t"; tot=0;} !/^[$$,\#+]/ { tot += $$1*$$2; } END {print tot;}' <$<); \
	awk -v total_num_distinct_kmers=$$TOTAL_NUM_DISTINCT_KMERS \
	    -v total_num_kmers=$$TOTAL_NUM_KMERS \
	'BEGIN{ printf "# 1:kmer_frequency\t2:num_distinct_kmers\t3:2/total_num_distinct_kmers\t4:cummulative(2)\t5:4/total_num_distinct_kmers\t6:num_kmers (1)*(2)\t7:6/total_num_kmers\t8:cummulative(6)\t9:8/total_num_kmers\n"; \
		_4=0; \
	        _8=0; \
	        FS=" "; \
	        OFS="\t"; } \
	!/^[$$,\#+]/ \
	{ kmer_frequency = $$1; \
	  num_distinct_kmers = $$2; \
	  _3 = num_distinct_kmers / total_num_distinct_kmers; \
	  _4 += num_distinct_kmers; \
	  _5 = _4 / total_num_distinct_kmers; \
	  _6 = kmer_frequency * num_distinct_kmers; \
	  _7 = _6 / total_num_kmers; \
	  _8 += _6; \
	  _9 = _8 / total_num_kmers; \
	  print kmer_frequency, num_distinct_kmers, _3, _4, _5, _6, _7, _8, _9; \
	}' <$< >$@



# # plot histogram in R
# jellyfish.histo.count.pdf: jellyfish.histo.count
# 	$(call module_loader); \
# 	 distrib_plot --type=histogram --breaks=$(MAX_OCC) --output=$@ 3 <$<




# more complete statistics
jellyfish.stats.verbose: jf_merged_$(KMER)
	!threads
	@echo
	@echo "++ Step 5: Running JELLYFISH to create statistics of k-mer occurence ..."
	@echo
	$(call module_loader); \
	jellyfish stats \
	-o $@ \
	$<


# dump lower counts (needed for seecer)
counts_$(KMER)_$(MIN_OCC): jf_merged_$(KMER)
	@echo
	@echo "++ Step 6: Running JELLYFISH to dump k-mer occurence ..."
	@echo
	$(call module_loader); \
	jellyfish dump --lower-count=$(MIN_OCC) -c $< -o $@;



# count only 16-mers. It is very fast because count in memory.
# produce output in 3 columns: take 1 and 3 for produce the histogram
NOTPARALLEL: kmers_count.hist
kmers_count.hist: $(SEQ_FASTQ) jellyfish.stats.verbose
	!threads
	$(call module_loader); \
	kmers_count $(addprefix --input ,$^) --threads $$THREADNUM --max-occ $(MAX_OCC) --16mer-hist $@


# print histogram
kmers_count.hist.pdf: kmers_count.hist
	$(call module_loader); \
	head -n -1 $< | distrib_plot --type=histogram --breaks=$(MAX_OCC) --output=$@ 3


# test_kmer_freq_pread:
# 	kmer_freq_pread -k 16 -l <(echo -e "0.fastq\n1.fastq\n2.fastq") -t 20 -p $@_k17  2>$@_k17.log

.PHONY: test
test:
	@echo $(wildcard *.fastq.N)



ALL +=	jf_merged_$(KMER) \
	jellyfish.histo \
	jellyfish.histo.count \
	jellyfish.stats.verbose
#	jellyfish.histo.count.pdf \
#	counts_$(KMER)_$(MIN_OCC) \




# ifeq ($(KMER),16)
# ALL +=	kmers_count.hist \
# 	kmers_count.hist.pdf
# endif


INTERMEDIATE += 

#$(wildcard *.fastq) \
#		$(SEQ_FASTQ) \
#		$(SEQ_FASTQ_N)

CLEAN +=