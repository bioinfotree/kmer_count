# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>


# TODO:


context prj/rnaseq_assembly

# remove count for kmers with frequence less then indicated
LCOUNT	?= 3
# kmers length
KMERS	?= 16 17 18 19


# recall bmake
MAKE := bmake


# Prepare directories and generate directories list
# setting up the variable DIR_LST
dir_lst.mk: makefile
	RULES_PATH="$$PRJ_ROOT/local/share/rules/merge_phase_1.1.mk"; \
	if [ -s $$RULES_PATH ] && [ -s $< ]; then \
	  printf "DIR_LST :=" >$@; \
	  for KMER in $(KMERS); do \
	    mkdir -p $$KMER; \   * create dir for given kmer*
	    cd $$KMER; \
	    ln -sf ../$<; \   * link makefile *
	    ln -sf $$RULES_PATH rules.mk; \   * link rules *
	    for PRJ in $(KMER_COUNT_PRJ); do \
			ln -sf  ../../../$$PRJ/phase_1/$(KMERS)/jf_merged_$(KMERS) jf_"$$PRJ"_"$$KMER"; \
	    done; \
	    cd ..; \
	    printf " $$KMER" >>$@; \   * add dir name to list *
	  done; \
	fi

include dir_lst.mk


# if not defined HASH_SIZE calulate it using the formula:
# (G + k * n)/0.8 where:
# G = estimated genome size
# k = kmer length
# n = number of reads
# ifndef HASH_SIZE
# # count the total number of reads
# READS = $(shell zcat *.fastq.gz | fastq2tab | wc -l)
# 
# # calulate using the formula
# HASH_SIZE = $(shell echo "($(G_SIZE) + $(KMER) * $(READS)) / 0.8 " | bc)
# endif


# prevent KMERS variable from being exported
unexport KMERS
# export SEQ_GZ_LN to sub-makefiles so they can know about the list
# of input files in their subdirectory

# execute bmake on each subdirectory
subsystem.flag: dir_lst.mk
	@echo Looking into subdirs: $(MAKE)
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) KMER=$$D && \   * initialize KMER varible of sub-makefile with appropriate value *
	    cd ..; \
	  fi; \
	done; \
	touch $@


.PHONY: test
test:
	@echo $(SEQ_GZ)






ALL += dir_lst.mk \
	$(DIR_LST) \
	subsystem.flag

INTERMEDIATE += 

CLEAN +=

# add dipendence to clean targhet
clean: clean_dir

# recall bmake clean into each subdirectory
.PHONY: clean_dir
clean_dir:
	@echo cleaning up...
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) clean; \
	    cd ..; \
	  fi; \
	done
